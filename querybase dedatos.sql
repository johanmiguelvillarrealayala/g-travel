CREATE DATABASE aerolinea;

USE aerolinea;

CREATE TABLE ciudad (
    id INT PRIMARY KEY IDENTITY (1, 1),
    nombre VARCHAR (50) NOT NULL
);

CREATE TABLE pais (
    id INT PRIMARY KEY IDENTITY (1, 1),
	nombre VARCHAR (50) NOT NULL
);

CREATE TABLE aeropuerto (
    id INT PRIMARY KEY IDENTITY (1, 1),
    nombre VARCHAR (50) NOT NULL,
    idCiudad INT NOT NULL,
	idPais INT NOT NULL,
    FOREIGN KEY (idCiudad) REFERENCES ciudad (id),
	FOREIGN KEY (idPais) REFERENCES pais (id)
);

CREATE TABLE agencia (
    id INT PRIMARY KEY IDENTITY (1, 1),
    rfc VARCHAR (50) NOT NULL,
    nombre VARCHAR (50) NOT NULL,
    direccion VARCHAR(80),
    idAeropuerto INT NOT NULL,
    FOREIGN KEY (idAeropuerto) REFERENCES aeropuerto (id)
);

CREATE TABLE hotel (
    id INT PRIMARY KEY IDENTITY (1, 1),
    rfc VARCHAR (50) NOT NULL,
    nombre VARCHAR (50) NOT NULL,
    categoria DATETIME,
    idCiudad INT NOT NULL,
    FOREIGN KEY (idCiudad) REFERENCES ciudad (id)
);

CREATE TABLE modelo(
	id INT PRIMARY KEY IDENTITY (1, 1),
	nombre VARCHAR(50) NOT NULL
);

CREATE TABLE marca(
	id INT PRIMARY KEY IDENTITY (1, 1),
	nombre VARCHAR(50) NOT NULL
);

CREATE TABLE aerolinea (
    id INT PRIMARY KEY IDENTITY (1, 1),
    nombre VARCHAR (50) NOT NULL,
    idAeropuerto INT NOT NULL,
    FOREIGN KEY (idAeropuerto) REFERENCES aeropuerto (id)
);

CREATE TABLE piloto (
    id INT PRIMARY KEY IDENTITY (1, 1),
    nombre VARCHAR (100) NOT NULL,
    edad INT  NOT NULL,
    correo VARCHAR (50) NOT NULL,
    numTelefono INT,
    idAerolinea INT NOT NULL,
    FOREIGN KEY (idAerolinea) REFERENCES aerolinea (id)
);


CREATE TABLE avion (
    id INT PRIMARY KEY IDENTITY (1, 1),
    marca INT NOT NULL,
    modelo INT NOT NULL,
	idPiloto INT NOT NULL,
    color VARCHAR (50),
    numPasajero INT NOT NULL,
    FOREIGN KEY (marca) REFERENCES marca (id),
	FOREIGN KEY (idPiloto) REFERENCES piloto (id),
	FOREIGN KEY (modelo) REFERENCES modelo (id)
);

CREATE TABLE ruta (
    id INT PRIMARY KEY IDENTITY (1, 1),
    ciudadDestino VARCHAR (50) NOT NULL,
    ciudadPartido VARCHAR (50) NOT NULL,
    idAvion INT NOT NULL,
    FOREIGN KEY (idAvion) REFERENCES avion (id)
);

CREATE TABLE pasajero (
    id INT PRIMARY KEY IDENTITY (1, 1),
    nombre VARCHAR (50) NOT NULL,
    pasaporte VARCHAR (50) NOT NULL,
    edad INT,
    genero VARCHAR(20) NOT NULL,
	fechaNacimiento DATETIME,
	correo VARCHAR (60) NOT NULL,
	numTelefono INT,
	direccion VARCHAR (60) NOT NULL,
	idAerolinea INT NOT NULL,
    idAvion INT NOT NULL,
    FOREIGN KEY (idAerolinea) REFERENCES aerolinea (id),
	FOREIGN KEY (idAvion) REFERENCES avion (id)
);

CREATE TABLE viaje (
    id INT PRIMARY KEY IDENTITY (1, 1),
    precio FLOAT NOT NULL,
    idRuta INT NOT NULL,
    idPasajero INT NOT NULL,
    FOREIGN KEY (idRuta) REFERENCES ruta (id),
	FOREIGN KEY (idPasajero) REFERENCES pasajero (id)
);

CREATE TABLE cargos (
    id INT PRIMARY KEY IDENTITY (1, 1),
    nombre VARCHAR (50) NOT NULL
);

CREATE TABLE permisos (
    id INT PRIMARY KEY IDENTITY (1, 1),
    nombre VARCHAR (50) NOT NULL
);

CREATE TABLE tipoUsuario (
    id INT PRIMARY KEY IDENTITY (1, 1),
    nombre VARCHAR (50) NOT NULL
);

CREATE TABLE detalleTipoUsuarioPermiso (
    id INT PRIMARY KEY IDENTITY (1, 1),
    nombre VARCHAR (50) NOT NULL,
	idTipoUsuario INT NOT NULL,
    idPermiso INT NOT NULL,
    FOREIGN KEY (idPermiso) REFERENCES permisos (id),
	FOREIGN KEY (idTipoUsuario) REFERENCES tipoUsuario (id) 
);

CREATE TABLE usuario (
    id INT PRIMARY KEY IDENTITY (1, 1),
    nombre VARCHAR (50) NOT NULL,
    correo VARCHAR (50) NOT NULL,
    contrasena VARCHAR (50) NOT NULL,
    idCargo INT NOT NULL,
	idDetalleTUsuarioP INT NOT NULL,
    FOREIGN KEY (idCargo) REFERENCES cargos (id),
	FOREIGN KEY (idDetalleTUsuarioP) REFERENCES detalleTipoUsuarioPermiso (id)
);